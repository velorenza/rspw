<?php
	
	$medical_hospital_theme_color = get_theme_mod('medical_hospital_theme_color');

	$custom_css = '';

	if($medical_hospital_theme_color != false){
		$custom_css .='input[type="submit"], a.button, #footer input[type="submit"], #comments input[type="submit"].submit, #comments a.comment-reply-link:hover, #sidebar .tagcloud a:hover, span.page-number,span.page-links-title, .nav-menu ul ul a, .social-media i:hover, #contact-us, h1.page-title, h1.search-title, .textbox a , .blogbtn a , .woocommerce span.onsale, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, nav.woocommerce-MyAccount-navigation ul li, .inner, .footerinner .tagcloud a, .bradcrumbs a , .readbutton a, #sidebar h3::before,  .pagination .current , span.meta-nav , .title-box, #sidebar input[type="submit"], .tags a:hover, #comments a.comment-reply-link, .main-service-box:nth-child(odd), .main-service-box:nth-child(even), .topbar .nav li a:hover{';
			$custom_css .='background-color: '.esc_html($medical_hospital_theme_color).';';
		$custom_css .='}';
	}
	if($medical_hospital_theme_color != false){
		$custom_css .='a, .nav-menu ul li a:active, #sidebar ul li a:hover, .nav-menu ul ul a:hover, .logo h1 a, .logo p.site-title a, .blog-sec h2 a, .main-service-box a i, .footerinner ul li a:hover, #sidebar h3 , span.post-title, .entry-content a,  .nav-menu .current_page_ancestor > a , .post-info i, #wrapper h1, .tags a i, .comment-meta.commentmetadata a,.nav-menu ul li a:hover {';
			$custom_css .='color: '.esc_html($medical_hospital_theme_color).';';
		$custom_css .='}';
	}
	if($medical_hospital_theme_color != false){
		$custom_css .=' a.button, #sidebar form,  #sidebar aside, .nav-menu ul ul, .tags a:hover, .pagination a:hover, .pagination .current,.nav-menu ul ul a:hover{';
			$custom_css .='border-color: '.esc_html($medical_hospital_theme_color).';';
		$custom_css .='}';
	}

	// Layout Options
	$theme_layout = get_theme_mod( 'medical_hospital_theme_layout_options','Default Theme');
    if($theme_layout == 'Default Theme'){
		$custom_css .='body{';
			$custom_css .='max-width: 100%;';
		$custom_css .='}';
	}else if($theme_layout == 'Container Theme'){
		$custom_css .='body{';
			$custom_css .='width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;';
		$custom_css .='}';
		$custom_css .='#contact-us{';
			$custom_css .='position:static;';
		$custom_css .='}';
	}else if($theme_layout == 'Box Container Theme'){
		$custom_css .='body{';
			$custom_css .='max-width: 1140px; width: 100%; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;';
		$custom_css .='}';
		$custom_css .='#contact-us{';
			$custom_css .='position:static;';
		$custom_css .='}';
	}


	/*--------------------------- Slider Opacity -------------------*/

	$slider_layout = get_theme_mod( 'medical_hospital_slider_opacity_color','0.7');
	if($slider_layout == '0'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0';
		$custom_css .='}';
	}else if($slider_layout == '0.1'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.1';
		$custom_css .='}';
	}else if($slider_layout == '0.2'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.2';
		$custom_css .='}';
	}else if($slider_layout == '0.3'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.3';
		$custom_css .='}';
	}else if($slider_layout == '0.4'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.4';
		$custom_css .='}';
	}else if($slider_layout == '0.5'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.5';
		$custom_css .='}';
	}else if($slider_layout == '0.6'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.6';
		$custom_css .='}';
	}else if($slider_layout == '0.7'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.7';
		$custom_css .='}';
	}else if($slider_layout == '0.8'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.8';
		$custom_css .='}';
	}else if($slider_layout == '0.9'){
		$custom_css .='#slider img{';
			$custom_css .='opacity:0.9';
		$custom_css .='}';
	}

	/*---------------------------Slider Content Layout -------------------*/

	$slider_layout = get_theme_mod( 'medical_hospital_slider_alignment_option','Center Align');
    if($slider_layout == 'Left Align'){
		$custom_css .='#slider .carousel-caption{';
			$custom_css .='text-align:left;';
		$custom_css .='}';
		$custom_css .='#slider .carousel-caption{';
		$custom_css .='left:15%; right:25%;';
		$custom_css .='}';
	}else if($slider_layout == 'Center Align'){
		$custom_css .='#slider .carousel-caption{';
			$custom_css .='text-align:center;';
		$custom_css .='}';
	}else if($slider_layout == 'Right Align'){
		$custom_css .='#slider .carousel-caption{';
			$custom_css .='text-align:right;';
		$custom_css .='}';
		$custom_css .='#slider .carousel-caption{';
		$custom_css .='left:25%; right:15%;';
		$custom_css .='}';
	}

	/*--------- Preloader Color Option -------*/
	$medical_hospital_preloader_color = get_theme_mod('medical_hospital_preloader_color');

	if($medical_hospital_preloader_color != false){
		$custom_css .=' .loader{';
			$custom_css .='border-color: '.esc_html($medical_hospital_preloader_color).';';
		$custom_css .='} ';
		$custom_css .=' .loader-inner{';
			$custom_css .='background-color: '.esc_html($medical_hospital_preloader_color).';';
		$custom_css .='} ';
	}

	$medical_hospital_preloader_bg_color = get_theme_mod('medical_hospital_preloader_bg_color');

	if($medical_hospital_preloader_bg_color != false){
		$custom_css .=' #overlayer{';
			$custom_css .='background-color: '.esc_html($medical_hospital_preloader_bg_color).';';
		$custom_css .='} ';
	}